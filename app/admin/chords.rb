ActiveAdmin.register Chord do
  index :as => :grid do |chord|
    link_to(image_tag(chord.image), admin_chords_path(chord))
  end
   
  form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs "Details" do
      f.input :name
    end
    f.buttons
   end
   
   show do
     h3 chord.name
     div do
     end
   end
end
