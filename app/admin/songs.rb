ActiveAdmin.register Song do
  show do
    h3 song.title
    div do
     song.lyrics.html_safe
    end
  end
  
  index do
    column :number
    column :title
    default_actions
  end
end
