class ConverterController < ApplicationController
  def index

    if params[:commit] == "Submeter" 
      @song = Song.new
      @song.title = params[:title]
      @song.number = params[:number]
      @song.lyrics = params[:lyrics]
      @song.author = params[:author]
      @song.save

      respond_to do |format|
        if @song.save
          format.html { redirect_to @song, notice: 'Song was successfully submited.' }
          format.json { render json: @song, status: :created, location: @song }
        end
      end

    else
      if params[:contents]
        value = params[:contents]
        value = value.gsub!(/\\beginverse\s*(% start verse)*/, '<div class="verse">')
        value = value.gsub!(/\\endverse\s*(% end verse)*/, "</div>")
        
        value = value.gsub!(/\\beginchorus\s*(% start chorus)*/, '<div class="chorus">')
        value = value.gsub!(/\\endchorus\s*(% end chorus)*/, "</div>")
        value = value.gsub!(/\\[\[]([a-z0-9#]*)[\]]/i, '<span class="chord \1">\1</span>')

        value = value.gsub!(/^(?=.*?%).*$/, "")
        value = value.gsub!(/\n{2,}/, '')
        value = value.gsub!(/(^(?!.*div*).*)$/, '<p>\1</p>')
        value = value.gsub!(/<p>[\s$]*<\/p>/, '')
        value = value.gsub!(/\s*\r/,'')
        value = value.gsub!(/\n\r/, "\r")

      end
    end
    
    @translation = Translation.new
  end
  
  def create
    @translation = Translation.new(params[:contents])

    respond_to do |format|
      if @translation.save
        format.html { redirect_to @song, notice: 'Song was successfully created.' }
        format.json { render json: @song, status: :created, location: @song }
      else
        format.html { render action: "new" }
        format.json { render json: @song.errors, status: :unprocessable_entity }
      end
    end
  end
end
