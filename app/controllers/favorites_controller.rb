class FavoritesController < ApplicationController
  
  def fav
    favorite = Favorite.find_or_initialize_by_user_id_and_song_id(current_user.id, params[:song_id])
    favorite.update_attributes({
      :user_id => current_user.id,
      :song_id => params[:song_id]
    })
    redirect_to(song_path(params[:song_id]))
  end
  
end
