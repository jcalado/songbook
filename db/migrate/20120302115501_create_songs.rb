class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.string :title
      t.integer :number
      t.text :lyrics
      t.boolean :enabled

      t.timestamps
    end
  end
end
